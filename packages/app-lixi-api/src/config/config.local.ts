export default {
  graphql: {
    playgroundEnabled: true,
    debug: true,
    schemaDestination: './schema.graphql',
    sortSchema: true
  }
};
