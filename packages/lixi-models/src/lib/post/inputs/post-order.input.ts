import { Field, InputType, registerEnumType } from '@nestjs/graphql';

import { Order } from '../../../core';

export enum PostOrderField {
  id = 'id',
  lotusBurnScore = 'lotusBurnScore',
  createdAt = 'createdAt',
  updatedAt = 'updatedAt',
  updatedRepostAt = 'updatedRepostAt',
  content = 'content'
}

registerEnumType(PostOrderField, {
  name: 'PostOrderField',
  description: 'Properties by which post connections can be ordered.'
});

@InputType()
export class PostOrder extends Order {
  @Field(() => PostOrderField)
  field: PostOrderField;
}
