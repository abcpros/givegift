# LixiLotus Monorepo

 <p align="center">
  <img alt="GitHub commit activity" src="https://img.shields.io/github/commit-activity/m/bcProFoundation/lixilotus">
  <a href="https://opensource.org/licenses/MIT/" target="_blank"><img alt="MIT License" src="https://img.shields.io/badge/License-MIT-blue.svg" style="display: inherit;"/></a>
  <img alt="GitHub contributors" src="https://img.shields.io/github/contributors/bcProFoundation/lixilotus">
  <br>
</p>

**Infrastructure to build an attention economy based on [Lotus](https://givelotus.org).**

## Applications

- [LixiLotus Api](packages/app-lixi-api) - A backend api provide the logic for lixi give away and lixilotus love programs
- [LixiLotus Web](packages/app-lixi) - A social network software built on blockchain for attention enonomy based on community moderation without inteference of bots and platforms.
- [SendLotus](packages/app-sendlotus) - A Lotus wallet

## Libraries

- [Lixi Models](packages/lixi-models) - A models library to use on both frontend and backend
- [Lixi Contract](packages/lixi-contracts) - Contains the contract to issue NFT on smartBCH
- [Lixi Components](packages/lixi-components) - A React library contains the components to use on the frontend

## License

Code released under [the MIT license](https://github.com/bcProFoundation/lixilotus/blob/master/LICENSE).
